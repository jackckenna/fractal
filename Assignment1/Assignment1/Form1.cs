﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get { return h; }
            }

            public float S
            {
                get { return s; }
            }

            public float B
            {
                get { return b; }
            }

            public int A
            {
                get { return a; }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;

                    float h = hsbColor.h * 360f / 255f;

                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }

                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private Bitmap picture, picture2;
        private Pen myPen;
        private bool mp = false;
        private Graphics g1;
        private Cursor c1, c2;
        

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
          
     
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          
            init();
            start();
        }

        private void Form1_Paint_1(object sender, PaintEventArgs e)
        {
            //g1 = e.Graphics;
            //pictureBox1.Image = picture;
            ////  g1.DrawImage(picture, 0, 0, x1, y1);
            //g1.Dispose();



        }

        //private HSB HSBcol = new HSB();


        private void pictureBox1_Click(object sender, EventArgs e)
        {


        }

        

        public void init() // all instances will be prepared
        {
            //HSBcol = new HSB();
            pictureBox1.Size = new Size(640, 480);
            finished = false;
            //addMouseListener(this);
            //addMouseMotionListener(this);
            c1 = Cursors.WaitCursor;
            c2 = Cursors.Cross;
            x1 = this.pictureBox1.Width;
            y1 = this.pictureBox1.Height;
            xy = (float)x1 / (float)y1;
            picture =  new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);
            pictureBox1.Image = picture;
            finished = true; 
        }



        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            Graphics g2 = e.Graphics;

            //assign the image to the appropriate graphics 
            if (picture2!=null)
            {
                g2.DrawImageUnscaled(picture2, 0, 0);
            }
            else
            {
                g2.DrawImageUnscaled(picture, 0, 0);
            }
            //sent the graphics to the update method
            paint(g2);

        }



        public void destroy() // delete all instances 
        {
            if (finished)
            {
                //reset values
                timer1.Enabled = false;
                // removeMouseListener(this);
                // removeMouseMotionListener(this);
                picture = null;
                picture2 = null;
                g1 = null;
                c1 = null;
                c2 = null;
                GC.Collect(); // garbage collection
            }
        }




        private void button3_Click(object sender, EventArgs e)
        {
            //restart button/ stop colorcycle button
            finished = true;
            destroy();
            init();
            start();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            //create save dialog 
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Jpeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog1.Title = "Save your fractal file here";
            DialogResult result = saveFileDialog1.ShowDialog();

            //check which image the user wants to save 
            if (result == DialogResult.OK) {

                if (picture2 != null) {
                    picture2.Save(saveFileDialog1.FileName);
                }
                else {
                    picture.Save(saveFileDialog1.FileName);
                }

                //send a messsage to the user
                MessageBox.Show("Your image has been Saved");
            }
            else
            {
                MessageBox.Show("Your image has not Saved");
            }
        }



        private void timer1_Tick(object sender, EventArgs e)

        {
            
            pictureChange();


        }

        private void pictureChange() {

            //create a memory stream to create a gif
            MemoryStream memoryStream = new MemoryStream();

            if (picture2 != null)
            {
                picture2.Save(memoryStream, ImageFormat.Gif); 
            }
            else
            {
                picture.Save(memoryStream, ImageFormat.Gif);
            }

            //get the gif from the memory stream 
            picture2 = new Bitmap(memoryStream);

            // create the palette 
            ColorPalette palette = picture2.Palette;
            Color start = palette.Entries[0];
            for (int i = 0; i < (palette.Entries.Length - 1); i++)
            {
                palette.Entries[i] = palette.Entries[i + 1];
            }
            palette.Entries[(palette.Entries.Length - 1)] = start;
            picture2.Palette = palette;
            //   picture = picture2;

            pictureBox1.Image = picture2;
            // pictureBox1.Invalidate();



        }

        private void button2_Click(object sender, EventArgs e)
        {
            //start the timer 
            timer1.Enabled = true;

        }

        private void zoomingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click and drag to zoom");
        }

        private void colorCyclingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Click on the Color Cycling button to begin, a sequence of fractals changing color will appear");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //play/pause timer
            if (timer1.Enabled)
            {
                timer1.Enabled = false;
               
            }
            else {
                timer1.Enabled = true;
            }
        }



        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();  
        }


        public void stop()
        {
        }


        public void paint(Graphics g)
        {
            update(g);
        }

        public void update(Graphics g)
        {

            //check if colorcycle is in use 
            if (picture2 != null)
            {
                g.DrawImage(picture2, 0, 0); 
            }
            else
            {
                g.DrawImage(picture, 0, 0);
            }
           
            //draw the rectangle 
            if (rectangle)
            {
                Pen pen = new Pen(Color.White);
                if (xs < xe)
                {
                    if (ys < ye) g.DrawRectangle(pen, xs, ys, (xe - xs), (ye - ys));
                    else g.DrawRectangle(pen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(pen, xe, ys, (xs - xe), (ye - ys));
                    else g.DrawRectangle(pen, xe, ye, (xs - xe), (ys - ye));
                }
                pen.Dispose();

         
               
            }
        }

        private void mandelbrot() // calculate all points
        {
            
            int x, y;
            float h, b, alt = 0.0f;
            action = false;
            Cursor.Current = c1;
            // setCursors(c1);

            toolStripStatusLabel1.Text = ("Mandelbrot-Set will be produced - please wait...");
            //showStatus("Mandelbrot-Set will be produced - please wait...");
            for (x = 0; x < x1; x += 2)


                for (y = 0; y < y1; y++)
                {

                  
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value

                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                                          ///djm added
                                          ///HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                                          ///Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                                          ///g1.setColor(col);
                                            //djm end
                                            //djm added to convert to RGB from HSB

                                            //g1.setColor(Color.getHSBColor(h, 0.8f, b));
                                            ////djm test
                                            //Color col = Color.getHSBColor(h, 0.8f, b);
                                            //int red = col.getRed();
                                            //int green = col.getGreen();
                                            //int blue = col.getBlue();
                                            //djm 
                        
                        Color col = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255)); //assign the color
                        myPen = new Pen(col);//pass it on to the pen 
                        alt = h;
                    }
                  g1.DrawLine(myPen, x, y, x + 1, y); //draw the madlebrot 
                }

            toolStripStatusLabel1.Text = ("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            //showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            Cursor.Current = c2;
          
            action = true;
           
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
                {
            // mouse is pressed
            mp = true;
                   //e.consume();
                   if (action)
                       {
                            xs = e.X;
                            ys = e.Y;
                       }
        }


        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
                {

            //mouse is not pressed 
            mp = false;
            int z, w;

            picture2 = null;
            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;

              
                mandelbrot();


                rectangle = false;
           

               pictureBox1.Image = picture2;
                pictureBox1.Invalidate();
            }

         }

         private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
            {
            //check if mouse is pressed
            if (mp)
            {
                //e.consume();
                if (action)
                {
                    xe = e.X;
                    ye = e.Y;
                    rectangle = true;
                    pictureBox1.Invalidate();

                }
            }

            }







        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
          
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
          
        }


    }
}